# Mon Projet

## Table des matières
- [Exercice 1 - Questions sur les acquis notions vues en cours](#exercice-1---questions-sur-les-acquis-notions-vues-en-cours)
- [Exercice 2](#exercice-2)
    - [Installer et lancer le projet](#installer-et-lancer-le-projet)

## Exercice 1 - Questions sur les acquis notions vues en cours

1. Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?
   Les principales sources de complexité dans un système logiciel sont :

Complexité algorithmique : la logique des algorithmes utilisés peut être difficile à comprendre et optimiser.
Complexité structurelle : la manière dont les différents composants du système sont organisés et interagissent entre eux.
Complexité des données : le format, la structure et la gestion des données peuvent ajouter des niveaux de complexité.
Complexité de l'interface utilisateur : concevoir des interfaces utilisateur intuitives et réactives peut être complexe.
Complexité environnementale : la gestion des dépendances, de l'infrastructure, et des divers environnements de déploiement.
Complexité des exigences : des exigences changeantes ou mal définies peuvent ajouter de la complexité au développement.

2. Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ? Vous pouvez illustrer votre réponse avec un code source minimal et/ou avec un diagramme.
   Programmer vers une interface et non vers une implémentation offre plusieurs avantages :

Flexibilité : il est plus facile de changer l'implémentation sans affecter le reste du code.
Testabilité : il est plus simple de créer des mocks ou des stubs pour les tests unitaires.
Réutilisabilité : les interfaces permettent de réutiliser le code plus facilement dans différents contextes.

Exemple de code : 

// Définition de l'interface
public interface PaymentProcessor {
    void processPayment(double amount);
}

// Implémentation concrète
public class CreditCardPaymentProcessor implements PaymentProcessor {
    @Override
    public void processPayment(double amount) {
    // Logique de paiement par carte de crédit
    }
}

// Utilisation de l'interface
public class ShoppingCart {
private PaymentProcessor paymentProcessor;

    public ShoppingCart(PaymentProcessor paymentProcessor) {
        this.paymentProcessor = paymentProcessor;
    }

    public void checkout(double amount) {
        paymentProcessor.processPayment(amount);
    }
}

// Dans l'application principale
    public class Main {
    public static void main(String[] args) {
    PaymentProcessor processor = new CreditCardPaymentProcessor();
    ShoppingCart cart = new ShoppingCart(processor);
    cart.checkout(100.0);
    }
}

3. Eric Steven Raymond, figure du mouvement open-source et hacker notoire, a récemment écrit sur le développement d’un système : “First make it run, next make it correct, and only after that worry about making it fast.
   ” que l’on peut traduire par “D’abord, faites en sorte que ça fonctionne, ensuite assurez-vous que ce soit correct, et seulement après, préoccupez-vous de le rendre rapide.
   ” Comment comprenez-vous chaque étape de cette heuristique ? Comment comprenez-vous cette heuristique dans son ensemble ?

First make it run : L'objectif initial est de créer une solution fonctionnelle, même si elle n'est pas parfaite ou optimisée. Cela permet de s'assurer que le concept de base est viable.
Next make it correct : Une fois que le système fonctionne, il faut s'assurer qu'il fonctionne correctement en respectant les exigences, en éliminant les bugs et en assurant la robustesse.
Only after that worry about making it fast : La dernière étape consiste à optimiser les performances. Il est inutile d'optimiser un système qui n'est pas fonctionnel ou correct.
Cette heuristique dans son ensemble suggère une approche itérative et progressive du développement logiciel, où chaque étape construit sur la précédente, minimisant les risques de régression et assurant une solution solide avant l'optimisation.

4. Nous avons vu en cours une technique pour lutter contre la déstructuration du code au cours du temps, le refactoring. Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?
   La méthode recommandée pour mener à bien un travail de refactoring inclut :

Tests unitaires : Assurer une couverture de tests adéquate avant de commencer le refactoring pour détecter rapidement les régressions.
Refactoring incrémental : Effectuer de petites modifications itératives et fréquemment tester le code.
Utiliser des outils de refactoring : Utiliser des IDE ou des outils spécialisés qui facilitent le refactoring en automatisant certaines tâches.
Suivre les principes de conception : Appliquer les principes SOLID, DRY (Don't Repeat Yourself), et KISS (Keep It Simple, Stupid).
Revue de code : Faire examiner le code refactoré par des pairs pour s'assurer que les modifications améliorent réellement la qualité du code.
Documenter les changements : Tenir à jour la documentation du code pour refléter les modifications apportées.
Cette approche structurée permet de maintenir la qualité du code tout en l'améliorant progressivement, réduisant les risques d'introduction de nouveaux bugs.


## Exercice 2

### Installer et lancer le projet
Pour installer et lancer le projet, suivez les étapes ci-dessous :

1. **Cloner le dépôt**
    ```bash
    git clone https://gitlab.com/TheronBenjamin/analyse-maintenabilite_t_benjamin
    cd dossier_concerné
    code . ou idea .  si vous disposez de l'ouverture de l'IDE en ligne de commande
    ```

2. **Installer les dépendances**
   *(Aucune dépendance particulière n'est nécessaire pour ce projet PHP)*

3. **Lancer le projet**
   Ouvrez les fichiers PHP dans votre serveur web local ou utilisez l'interface de ligne de commande PHP :
    ```bash
    php -S localhost:8000
    ```
   Ensuite, accédez à `http://localhost:8000` dans votre navigateur.


---