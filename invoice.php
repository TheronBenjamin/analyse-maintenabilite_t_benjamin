<?php

interface PricingStrategy
{
    public function calculateAmount(int $daysRented): float;
}

class RegularPricingStrategy implements PricingStrategy
{
    public function calculateAmount(int $daysRented): float
    {
        $amount = 5000 + $daysRented * 9500;
        if ($daysRented > 5) {
            $amount -= ($daysRented - 2) * 10000;
        }
        return $amount;
    }
}

class NewModelPricingStrategy implements PricingStrategy
{
    public function calculateAmount(int $daysRented): float
    {
        $amount = 9000 + $daysRented * 15000;
        if ($daysRented > 3) {
            $amount -= ($daysRented - 2) * 10000;
        }
        return $amount;
    }
}

class Car
{
    private string $_title;
    private PricingStrategy $_pricingStrategy;

    public function __construct(string $title, PricingStrategy $pricingStrategy)
    {
        $this->_title = $title;
        $this->_pricingStrategy = $pricingStrategy;
    }

    public function getTitle(): string
    {
        return $this->_title;
    }

    public function calculateAmount(int $daysRented): float
    {
        return $this->_pricingStrategy->calculateAmount($daysRented);
    }
}

class Rental
{
    private Car $_Car;
    private int $_daysRented;

    public function __construct(Car $Car, int $daysRented)
    {
        $this->_Car = $Car;
        $this->_daysRented = $daysRented;
    }

    public function getDaysRented(): int
    {
        return $this->_daysRented;
    }

    public function getCar(): Car
    {
        return $this->_Car;
    }
}

class Customer
{
    private string $_name;
    private array $_rentals = [];

    public function __construct(string $name)
    {
        $this->_name = $name;
    }

    public function addRental(Rental $arg): void
    {
        $this->_rentals[] = $arg;
    }

    public function getName(): string
    {
        return $this->_name;
    }

    public function getRentals(): array
    {
        return $this->_rentals;
    }
}

class InvoiceGenerator
{
    public function generate(Customer $customer, string $format = 'text'): string
    {
        if ($format === 'json') {
            return $this->generateJsonInvoice($customer);
        }
        return $this->generateTextInvoice($customer);
    }

    private function generateTextInvoice(Customer $customer): string
    {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $result = "Rental Record for " . $customer->getName() . "\n";

        foreach ($customer->getRentals() as $each) {
            $thisAmount = $each->getCar()->calculateAmount($each->getDaysRented());

            $frequentRenterPoints++;

            if ($each->getCar()->calculateAmount() instanceof NewModelPricingStrategy && $each->getDaysRented() > 1) {
                $frequentRenterPoints++;
            }

            $result .= "\t" . $each->getCar()->getTitle() . "\t" . number_format($thisAmount / 100, 1) . "\n";
            $totalAmount += $thisAmount;
        }

        $frequentRenterPoints += floor($totalAmount / 1000);

        $result .= "Amount owed is " . number_format($totalAmount / 100, 1) . "\n";
        $result .= "You earned " . $frequentRenterPoints . " frequent renter points\n";

        return $result;
    }

    private function generateJsonInvoice(Customer $customer): string
    {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $invoice = [
            'customer' => $customer->getName(),
            'rentals' => []
        ];

        foreach ($customer->getRentals() as $each) {
            $thisAmount = $each->getCar()->calculateAmount($each->getDaysRented());

            $frequentRenterPoints++;

            if ($each->getCar()->calculateAmount() instanceof NewModelPricingStrategy && $each->getDaysRented() > 1) {
                $frequentRenterPoints++;
            }

            $invoice['rentals'][] = [
                'title' => $each->getCar()->getTitle(),
                'amount' => $thisAmount / 100
            ];

            $totalAmount += $thisAmount;
        }

        $frequentRenterPoints += floor($totalAmount / 1000);

        $invoice['totalAmount'] = $totalAmount / 100;
        $invoice['frequentRenterPoints'] = $frequentRenterPoints;

        return json_encode($invoice, JSON_PRETTY_PRINT);
    }
}

$regularPricing = new RegularPricingStrategy();
$newModelPricing = new NewModelPricingStrategy();

$Car1 = new Car("Car 1", $regularPricing);
$Car2 = new Car("Car 2", $newModelPricing);

$rental1 = new Rental($Car1, 3);
$rental2 = new Rental($Car2, 2);

$customer = new Customer("John Doe");
$customer->addRental($rental1);
$customer->addRental($rental2);

$invoiceGenerator = new InvoiceGenerator();
echo $invoiceGenerator->generate($customer, 'text');
echo $invoiceGenerator->generate($customer, 'json');
